<?php

/**
 * @file
 *  Migrations for MNS Events.
 */

class DFSMNSNewsNodes extends ImportBaseNodes {

  public function __construct() {
    parent::__construct();
    $this->description = t('Import nodes.');

    $import_path = drupal_get_path('module', 'dfs_mns') . '/import/';

    // Create a MigrateSource object.
    $this->source = new MigrateSourceCSV($import_path . 'dfs_mns.nodes.mns_news.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationNode('news');

    // Image
    $this->addFieldMapping('field_image', 'image');
    $this->addFieldMapping('field_image:file_replace')->defaultValue(FILE_EXISTS_REPLACE);
    $this->addFieldMapping('field_image:source_dir')->defaultValue($import_path . 'images');
    $this->addFieldMapping('field_image:destination_file', 'filename');

    $this->addFieldMapping('field_event_date', 'date');
    $this->addFieldMapping('field_landing_tags', 'tags');
    $this->addFieldMapping('og_group_ref', 'group')->sourceMigration('DFSMNSGroupNodes');
    $this->addFieldMapping('workbench_moderation_state_new', 'workbench_moderation_state_new')->defaultValue('published');
  }

  function csvcolumns() {
    $columns[0] = array('title', 'Title');
    $columns[1] = array('body', 'Body');
    $columns[2] = array('tags', 'Tags');
    $columns[3] = array('image', 'Image');
    $columns[4] = array('group', 'Group');
    return $columns;
  }

  function prepareRow($row) {
    $row->tags = explode(", ", $row->tags);
    return TRUE;
  }
}

<?php

/**
 * @file
 *  Migrations for WEM Contest Nodes.
 */

class DFSMNSGroupNodes extends ImportBaseNodes {

  public function __construct() {
    parent::__construct();
    $this->description = t('Import nodes.');

    $import_path = drupal_get_path('module', 'dfs_mns') . '/import/';

    // Create a MigrateSource object.
    $this->source = new MigrateSourceCSV($import_path . 'dfs_mns.nodes.mns_member_group.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationNode('member_group');

    $this->addFieldMapping('workbench_moderation_state_new', 'workbench_moderation_state_new')->defaultValue('published');
  }

  function csvcolumns() {
    $columns[0] = array('title', 'Title');
    $columns[1] = array('body', 'Body');
    return $columns;
  }

}


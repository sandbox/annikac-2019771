<?php

/**
 * @file
 *  Migrations for MNS Events.
 */

class DFSMNSEventNodes extends ImportBaseNodes {

  public function __construct() {
    parent::__construct();
    $this->description = t('Import nodes.');

    $import_path = drupal_get_path('module', 'dfs_mns') . '/import/';

    // Create a MigrateSource object.
    $this->source = new MigrateSourceCSV($import_path . 'dfs_mns.nodes.mns_event.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationNode('event');

    $this->addFieldMapping('field_event_date', 'date');
    $this->addFieldMapping('og_group_ref', 'group')->sourceMigration('DFSMNSGroupNodes');
    $this->addFieldMapping('workbench_moderation_state_new', 'workbench_moderation_state_new')->defaultValue('published');
  }

  function csvcolumns() {
    $columns[0] = array('title', 'Title');
    $columns[1] = array('body', 'Body');
    $columns[2] = array('date', 'Date');
    $columns[3] = array('group', 'Group');
    return $columns;
  }

}

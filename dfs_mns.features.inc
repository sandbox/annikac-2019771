<?php
/**
 * @file
 * dfs_mns.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function dfs_mns_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function dfs_mns_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function dfs_mns_node_info() {
  $items = array(
    'event' => array(
      'name' => t('Event'),
      'base' => 'node_content',
      'description' => t('Create an event.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'member_group' => array(
      'name' => t('Member Group'),
      'base' => 'node_content',
      'description' => t('Creates a member group, for users interested in a particular area'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'news' => array(
      'name' => t('News'),
      'base' => 'node_content',
      'description' => t('Create news. News can also be assigned to a group to be displayed within the group page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}

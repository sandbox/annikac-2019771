<?php

/**
 *  @file
 *   Contains user migration class for DFS MNS
 */

class DFSMNSUsers extends ImportBaseUsers {

  public function __construct() {
    parent::__construct();
    $this->description = t('Import DFS MNS Users');
    $import_path = drupal_get_path('module', 'dfs_mns') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dfs_mns.users.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->addFieldMapping('picture', 'picture')->sourceMigration('DFSMNSUserPictures');
  }

  function csvcolumns() {
    $columns[0] = array('name', 'Name');
    $columns[1] = array('pass', 'Pass');
    $columns[2] = array('mail', 'Mail');
    $columns[3] = array('status', 'Status');
    $columns[4] = array('roles', 'Roles');
    $columns[5] = array('picture', 'Picture');
    return $columns;
  }

  public function prepareRow($row) {
    $user_roles = explode(", ", $row->roles);
    $roles = array('2' => '2');
    foreach ($user_roles as $role_name) {
      $rid = db_query('SELECT rid FROM {role} WHERE name = :name', array(':name' => $role_name))->fetchField();
      $roles[$rid] = $rid;
    }
    $row->roles = $roles;
    return TRUE;
  }

}

class DFSMNSUserPictures extends ImportBaseUserPictures {
  public function __construct() {
    parent::__construct();
    $import_path = drupal_get_path('module', 'dfs_mns') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dfs_mns.users.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->addFieldMapping('source_dir')->defaultValue($import_path . 'images');
  }
  function csvcolumns() {
    $columns[5] = array('picture', 'Picture');
    return $columns;
  }
}

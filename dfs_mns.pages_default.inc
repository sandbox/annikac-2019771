<?php
/**
 * @file
 * dfs_mns.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function dfs_mns_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'df_mns_scenario_home';
  $page->task = 'page';
  $page->admin_title = 'DF MNS Scenario Home';
  $page->admin_description = '';
  $page->path = 'df-mns-home';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_df_mns_scenario_home_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'df_mns_scenario_home';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'whelan';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'column1' => NULL,
      'column2' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '675ab5b8-5a6b-4b8e-a812-d94033378e9d';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-c3506abe-823c-4802-9f6d-c575b9556134';
    $pane->panel = 'column1';
    $pane->type = 'views';
    $pane->subtype = 'list_site_groups';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '5',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array();
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'c3506abe-823c-4802-9f6d-c575b9556134';
    $display->content['new-c3506abe-823c-4802-9f6d-c575b9556134'] = $pane;
    $display->panels['column1'][0] = 'new-c3506abe-823c-4802-9f6d-c575b9556134';
    $pane = new stdClass();
    $pane->pid = 'new-e2c8d15e-d1c3-4024-89c5-b879376f99a8';
    $pane->panel = 'column1';
    $pane->type = 'menu_tree';
    $pane->subtype = 'user-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'menu_name' => 'user-menu',
      'parent_mlid' => 0,
      'parent' => 'user-menu:0',
      'title_link' => 0,
      'admin_title' => '',
      'level' => '1',
      'follow' => 0,
      'depth' => 0,
      'expanded' => 0,
      'sort' => 0,
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array();
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'e2c8d15e-d1c3-4024-89c5-b879376f99a8';
    $display->content['new-e2c8d15e-d1c3-4024-89c5-b879376f99a8'] = $pane;
    $display->panels['column1'][1] = 'new-e2c8d15e-d1c3-4024-89c5-b879376f99a8';
    $pane = new stdClass();
    $pane->pid = 'new-5c0c8122-6c13-4a41-923e-815b12b47a9c';
    $pane->panel = 'column1';
    $pane->type = 'block';
    $pane->subtype = 'user-login';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array();
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '5c0c8122-6c13-4a41-923e-815b12b47a9c';
    $display->content['new-5c0c8122-6c13-4a41-923e-815b12b47a9c'] = $pane;
    $display->panels['column1'][2] = 'new-5c0c8122-6c13-4a41-923e-815b12b47a9c';
    $pane = new stdClass();
    $pane->pid = 'new-da7311eb-4582-4fe7-b152-f0284b51a2f7';
    $pane->panel = 'column2';
    $pane->type = 'block';
    $pane->subtype = 'views--exp-group_events-page_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'inherit_path' => 0,
      'override_title' => 1,
      'override_title_text' => 'Search Events',
    );
    $pane->cache = array();
    $pane->style = array();
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'da7311eb-4582-4fe7-b152-f0284b51a2f7';
    $display->content['new-da7311eb-4582-4fe7-b152-f0284b51a2f7'] = $pane;
    $display->panels['column2'][0] = 'new-da7311eb-4582-4fe7-b152-f0284b51a2f7';
    $pane = new stdClass();
    $pane->pid = 'new-bb43149f-fd2d-4556-bf71-9f95471a167d';
    $pane->panel = 'column2';
    $pane->type = 'views';
    $pane->subtype = 'group_events';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '5',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'bb43149f-fd2d-4556-bf71-9f95471a167d';
    $display->content['new-bb43149f-fd2d-4556-bf71-9f95471a167d'] = $pane;
    $display->panels['column2'][1] = 'new-bb43149f-fd2d-4556-bf71-9f95471a167d';
    $pane = new stdClass();
    $pane->pid = 'new-9fd9b5bb-4acb-4ec6-927a-0156adda5bfa';
    $pane->panel = 'contentmain';
    $pane->type = 'views';
    $pane->subtype = 'group_news';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '5',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_1',
      'override_title' => 1,
      'override_title_text' => 'Latest News',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '9fd9b5bb-4acb-4ec6-927a-0156adda5bfa';
    $display->content['new-9fd9b5bb-4acb-4ec6-927a-0156adda5bfa'] = $pane;
    $display->panels['contentmain'][0] = 'new-9fd9b5bb-4acb-4ec6-927a-0156adda5bfa';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['df_mns_scenario_home'] = $page;

  return $pages;

}
